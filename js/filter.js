var userdata = [
  {
    userId: 1,
    firstName: "Krish",
    lastName: "Lee",
    phoneNumber: "123456",
    emailAddress: "krish.lee@learningcontainer.com"
  },
  {
    userId: 2,
    firstName: "racks",
    lastName: "jacson",
    phoneNumber: "123456",
    emailAddress: "racks.jacson@learningcontainer.com"
  },
  {
    userId: 3,
    firstName: "denial",
    lastName: "roast",
    phoneNumber: "33333333",
    emailAddress: "denial.roast@learningcontainer.com"
  },
  {
    userId: 4,
    firstName: "devid",
    lastName: "neo",
    phoneNumber: "222222222",
    emailAddress: "devid.neo@learningcontainer.com"
  },
  {
    userId: 5,
    firstName: "jone",
    lastName: "mac",
    phoneNumber: "111111111",
    emailAddress: "jone.mac@learningcontainer.com"
  },
  {
    userId: 6,
    firstName: "Krish 2",
    lastName: "Lee",
    phoneNumber: "123456",
    emailAddress: "krish.lee@learningcontainer.com"
  },
  {
    userId: 7,
    firstName: "racks 2",
    lastName: "jacson",
    phoneNumber: "123456",
    emailAddress: "racks.jacson@learningcontainer.com"
  },
  {
    userId: 8,
    firstName: "denial 2",
    lastName: "roast",
    phoneNumber: "33333333",
    emailAddress: "denial.roast@learningcontainer.com"
  },
  {
    userId: 9,
    firstName: "devid 2",
    lastName: "neo",
    phoneNumber: "222222222",
    emailAddress: "devid.neo@learningcontainer.com"
  },
  {
    userId: 10,
    firstName: "devid 2",
    lastName: "neo",
    phoneNumber: "222222222",
    emailAddress: "devid.neo@learningcontainer.com"
  },
  {
    userId: 11,
    firstName: "jone 2",
    lastName: "mac",
    phoneNumber: "111111111",
    emailAddress: "jone.mac@learningcontainer.com"
  }
]
var itemsLength = userdata.length;
var numberItemsPerPage = 5;
var paginationItemsLenth;

const cargarItems = (min, max) => {
  let requireData = userdata.slice(min, max);
  for (let index = 0; index < requireData.length; index++) {
    const element = requireData[index];
    let userData = document.getElementById('user-data');
    userData.innerHTML += `
      <li> ${element.userId} ${element.firstName} </li>
    `;
  }
}

if (itemsLength % 2 !== 0) {
  paginationItemsLenth = Math.floor(itemsLength / numberItemsPerPage) + 1;
} else {
  paginationItemsLenth;
}

for (var i = 0; i < paginationItemsLenth; i++) {
  console.log(typeof (i))
  var itemNumber = i + 1;
  var paginationItems = document.getElementById('pagination-items');
  paginationItems.innerHTML += `
    <li item-number='${itemNumber}' class='pagination-item'> ${itemNumber} </li> 
  `; 
}

var itemPagination = document.querySelectorAll('ul.pagination-items li');

for (let index = 0; index < itemPagination.length; index++) {
  const element = itemPagination[index];
  element.addEventListener('click', ($event) => {
    let userDataSelector = document.querySelectorAll('ul.user-data li');
    console.log(userDataSelector);
    for (let index = 0; index < userDataSelector.length; index++) {
      const element = userDataSelector[index];
      element.style.display = 'none';
    }
    var pageNumber = $event.path[0].attributes[0].value;

    var showFrom = numberItemsPerPage * (pageNumber - 1);
    var showTo = showFrom + numberItemsPerPage;
    cargarItems(showFrom, showTo);
  })

}

//appendSelectedComments(0, 5)
cargarItems(0, 5)