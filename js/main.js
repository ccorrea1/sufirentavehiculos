const concesionarios = [
    {
        id: 1,
        concesionario: 'altomotores',
        asesor: 'Sharon Cortés Charris​',
        email: 'shcorte@bancolombia.com.co',
        vitrina: 'Almotores Calle 9',
        marcas: 'Kia',
        direccion: 'Calle 9 No. 39 - 00',
        ciudad: 'Cali'
    },
    {
        id: 2,
        concesionario: 'altomotores',
        asesor: 'Natalia Jurado Pérez​',
        email: 'natjurad@bancolombia.com.co',
        vitrina: 'Almotores Sede Sur',
        marcas: 'Kia',
        direccion: 'Avenida Pasoancho No. 77 -109',
        ciudad: 'Cali'
    },
    {
        id: 3,
        concesionario: 'altomotores',
        asesor: 'Daniel Messa Bryon​​',
        email: 'dmessa@bancolombia.com.co',
        vitrina: 'Almotores Sede Norte',
        marcas: 'Kia',
        direccion: 'Calle 70 Norte No. 2A - 280',
        ciudad: 'Cali'
    },
    {
        id: 4,
        concesionario: 'armotor',
        asesor: 'Julian Andres Ibarguen Castañeda​​',
        email: 'jibargue@bancolombia.com.co',
        vitrina: 'Armotor Pereira',
        marcas: 'Kia - Honda',
        direccion: 'AVENIDA 30 DE AGOSTO # 93-41 VÍA BELMONTE',
        ciudad: 'Pereira'
    },
    {
        id: 5,
        concesionario: 'armotor',
        asesor: 'Jorge Duvan Hurtado Oviedo​​',
        email: 'jorghurt@bancolombia.com.co',
        vitrina: 'Armotor Armenia ',
        marcas: 'Kia - Honda',
        direccion: 'CRA 14 #28NORTE 60',
        ciudad: 'Armenia'
    },
    {
        id: 6,
        concesionario: 'armotor',
        asesor: 'Nathalia Valderrama Vega​​',
        email: 'nvalderr@bancolombia.com.co',
        vitrina: 'Armotor Manizales',
        marcas: 'Kia - Honda',
        direccion: 'CRA 23 # 35A11',
        ciudad: 'Manizales'
    },
    {
        id: 7,
        concesionario: 'orion',
        asesor: 'Raul Ledesma Leon​​',
        email: 'raledesm@bancolombia.com.co',
        vitrina: 'Auto Orión Sur',
        marcas: 'Kia',
        direccion: 'CRA 100 # 12 - 90',
        ciudad: 'Cali'
    },
    {
        id: 8,
        concesionario: 'orion',
        asesor: 'Adriana Marcela Vega Benavides',
        email: 'admvega@bancolombia.com.co',
        vitrina: 'Auto Orión Norte',
        marcas: 'Kia',
        direccion: 'AVDA 3 NORTE # 34 - 46',
        ciudad: 'Cali'
    },
    {
        id: 9,
        concesionario: 'autolarte',
        asesor: 'Alejandra Marcela Bolivar Vargas​',
        email: 'alboliva@bancolombia.com.co',
        vitrina: 'Autolarte Palacé',
        marcas: 'Chevrolet',
        direccion: 'Cra. 50 No. 32 -16 Palacé',
        ciudad: 'Medellín'
    },
    {
        id: 10,
        concesionario: 'autolarte',
        asesor: 'Alejandra Marcela Bolivar Vargas​',
        email: 'alboliva@bancolombia.com.co',
        vitrina: 'Autolarte Itagüí',
        marcas: 'Chevrolet',
        direccion: 'Cra. 42 No. 85 - 05 Autopista Sur',
        ciudad: 'Itagüí'
    },
    {
        id: 11,
        concesionario: 'autolarte',
        asesor: 'Diana Vanesa Jaramillo Castro​',
        email: 'vajarami@bancolombia.com.co',
        vitrina: 'Autolarte Rionegro',
        marcas: 'Chevrolet',
        direccion: 'Cra. 50 No. 44A - 32',
        ciudad: 'Rionegro'
    },
    {
        id: 12,
        concesionario: 'autolarte',
        asesor: 'Alejandra Marcela Bolivar Vargas​​',
        email: 'alboliva@bancolombia.com.co',
        vitrina: 'Autolarte Bello',
        marcas: 'Chevrolet',
        direccion: 'Diagonal 51 No. 42 - 20',
        ciudad: 'Bello'
    },
    {
        id: 13,
        concesionario: 'autolarte',
        asesor: 'Alejandra Marcela Bolivar Vargas​​',
        email: 'alboliva@bancolombia.com.co',
        vitrina: 'Autolarte Envigado',
        marcas: 'Chevrolet',
        direccion: 'Cra. 48 No. 32B Sur 139 - Local 346',
        ciudad: 'Envigado'
    },
    {
        id: 14,
        concesionario: 'autolux',
        asesor: 'Juan Carlos Uribe Marín',
        email: 'jucuribe@bancolombia.com.co',
        vitrina: 'Autolux',
        marcas: 'Volvo',
        direccion: 'Calle 29 # 43A -  5',
        ciudad: 'Medellín'
    },
    {
        id: 15,
        concesionario: 'automontaña',
        asesor: 'Juan Carlos Uribe Marín',
        email: 'jucuribe@bancolombia.com.co',
        vitrina: 'Automontaña',
        marcas: 'Mazda',
        direccion: 'Calle 29 # 43A - 5',
        ciudad: 'Medellín'
    },
    {
        id: 16,
        concesionario: 'automontaña',
        asesor: 'Jhon Alejandro Alzate Cardona',
        email: 'vajarami@bancolombia.com.co',
        vitrina: 'Automontaña Mayorca',
        marcas: 'Mazda',
        direccion: 'Calle 51 Sur 48 - 57 Local 4057- Centro Comercial Mayorca',
        ciudad: 'Medellín'
    },
    {
        id: 17,
        concesionario: 'barumotors',
        asesor: 'Sandra Milena Long Torrens',
        email: 'SLONG@bancolombia.com.co',
        vitrina: 'Barú Motors Cartagena',
        marcas: 'Ford',
        direccion: 'PIE DEL CERRO CLL 30 N. 19-64',
        ciudad: 'Cartagena'
    },
    {
        id: 18,
        concesionario: 'barumotors',
        asesor: 'Fernando José Londoño Guzmán',
        email: 'FERLONDO@BANCOLOMBIA.COM.CO',
        vitrina: 'Barú Motors Montería',
        marcas: 'Ford',
        direccion: 'Cra 2 No. 44-50',
        ciudad: 'Montería'
    },
    {
        id: 19,
        concesionario: 'janamotors',
        asesor: 'Viviana Marcela Fortich Guzman',
        email: 'VFORTICH@bancolombia.com.co',
        vitrina: 'Janna Motors Ford',
        marcas: 'Ford',
        direccion: 'Vía 40 # 69 - 40',
        ciudad: 'Barranquilla'
    },
    {
        id: 20,
        concesionario: 'janamotors',
        asesor: 'Pablo Vicente Plata  Prada',
        email: 'PPLATA@bancolombia.com.co',
        vitrina: 'Janna Motors Valledupar',
        marcas: 'Ford',
        direccion: 'Cra. 7a # 21 - 89',
        ciudad: 'Valledupar'
    },
    {
        id: 21,
        concesionario: 'janamotors',
        asesor: 'Pablo Vicente Plata  Prada',
        email: 'PPLATA@bancolombia.com.co',
        vitrina: 'Janna Motors Santa Marta',
        marcas: 'Ford',
        direccion: 'Av. El Libertador # 20-80',
        ciudad: 'Santa Marta'
    },
    {
        id: 22,
        concesionario: 'janamotors',
        asesor: 'Ahimeth Inmaculada Hernandez Escobar',
        email: 'AHHERNAN@bancolombia.com.co',
        vitrina: 'Janna Motors Mazda',
        marcas: 'Mazda',
        direccion: 'Cra. 54 # Calle 76 esquina',
        ciudad: 'Barranquilla'
    },
    {
        id: 23,
        concesionario: 'jorgecortes',
        asesor: 'Leidi Paola Portilla Cortés',
        email: 'lportill@bancolombia.com.co',
        vitrina: 'Jorge Cortés Mazda Castellana',
        marcas: 'Mazda',
        direccion: 'Tv. 55 #No. 97 a 60',
        ciudad: 'Bogotá'
    },
    {
        id: 24,
        concesionario: 'jorgecortes',
        asesor: 'Andrés Stivens Vargas Hernández',
        email: 'asvargas@bancolombia.com.co',
        vitrina: 'Jorge Cortés Mazda Castellana',
        marcas: 'Mazda',
        direccion: 'Tv. 55 #No. 97 a 60',
        ciudad: 'Bogotá'
    },
    {
        id: 25,
        concesionario: 'jorgecortes',
        asesor: 'Sandra Milena Estrada Grisales',
        email: 'sanestra@bancolombia.com.co',
        vitrina: 'Jorge Cortés Mazda Castellana',
        marcas: 'Mazda',
        direccion: 'Tv. 55 #No. 97 a 60',
        ciudad: 'Bogotá'
    },
    {
        id: 26,
        concesionario: 'jorgecortes',
        asesor: 'Andrés Stivens Vargas Hernández',
        email: 'asvargas@bancolombia.com.co',
        vitrina: 'Jorge Cortés Punto Rosales',
        marcas: 'Mazda',
        direccion: 'Cra. 7 No. 75- 21',
        ciudad: 'Bogotá'
    },
    {
        id: 27,
        concesionario: 'jorgecortes',
        asesor: 'Sandra Milena Estrada Grisales',
        email: 'sanestra@bancolombia.com.co',
        vitrina: 'Jorge Cortés Mazda Castellana 8',
        marcas: 'Mazda',
        direccion: 'Av. Carrera 70 # 99-55',
        ciudad: 'Bogotá'
    },
    {
        id: 28,
        concesionario: 'jorgecortes',
        asesor: 'Susana Fajardo Orozco',
        email: 'sforozc@bancolombia.com.co',
        vitrina: 'Jorge Cortés Ford Morato 3',
        marcas: 'Ford',
        direccion: 'Av. Carrera 70 # 99-55',
        ciudad: 'Bogotá'
    },
    {
        id: 29,
        concesionario: 'jorgecortes',
        asesor: 'Susana Fajardo Orozco',
        email: 'sforozc@bancolombia.com.co',
        vitrina: 'Jorge Cortés Kia Morato',
        marcas: 'Kia',
        direccion: 'Av. Carrera 70 # 99-55',
        ciudad: 'Bogotá'
    },
    {
        id: 30,
        concesionario: 'jorgecortes',
        asesor: 'Susana Fajardo Orozco',
        email: 'sforozc@bancolombia.com.co',
        vitrina: 'Jorge Cortés Punto Morato',
        marcas: 'Ford - Kia',
        direccion: 'Av. Carrera 70 # 99-31',
        ciudad: 'Bogotá'
    },
    {
        id: 31,
        concesionario: 'juanautos',
        asesor: 'Dominga del Carmen Morales Garcia',
        email: 'ddcarmen@bancolombia.com.co',
        vitrina: 'Juanautos Renault Shopping',
        marcas: 'Renault',
        direccion: 'Centro Cial Shopping Center La Plazuela, Local 18',
        ciudad: 'Cartagena'
    },
    {
        id: 32,
        concesionario: 'juanautos',
        asesor: 'Silvana Patricia Villalba Serrano',
        email: 'svillalb@bancolombia.com.co',
        vitrina: 'Juanautos Principal Renault',
        marcas: 'Renault',
        direccion: 'Pie del Cerro Calle 30 No. 18A - 104',
        ciudad: 'Cartagena'
    },
    {
        id: 33,
        concesionario: 'juanautos',
        asesor: 'Sandra Milena Long Torrens',
        email: 'SLONG@bancolombia.com.co',
        vitrina: 'Juanautos El Cerro Toyota',
        marcas: 'Toyota',
        direccion: 'Pie del Cerro - Av. del Lago No. 18A - 74',
        ciudad: 'Cartagena'
    },
    {
        id: 34,
        concesionario: 'massymotor',
        asesor: 'Natalia Jurado Pérez​',
        email: 'natjurad@bancolombia.com.co',
        vitrina: 'Massy Motor Premium',
        marcas: 'Mercedes Benz',
        direccion: 'CRA 8 # 33 - 16',
        ciudad: 'Cali'
    },
    {
        id: 35,
        concesionario: 'massymotor',
        asesor: 'Natalia Jurado Pérez​',
        email: 'natjurad@bancolombia.com.co',
        vitrina: 'Massy Motor Premium Ciudad Jardín',
        marcas: 'Mercedes Benz',
        direccion: 'CALLE 18 # 105 - 82',
        ciudad: 'Cali'
    },
    {
        id: 36,
        concesionario: 'mazko',
        asesor: 'Raul Ledesma Leon',
        email: 'raledesm@bancolombia.com.co',
        vitrina: 'Mazko Ciudad Jardín',
        marcas: 'Mazda',
        direccion: 'CRA 100 # 12 - 90',
        ciudad: 'Cali'
    },
    {
        id: 37,
        concesionario: 'moevo',
        asesor: 'Diana Vanesa Jaramillo Castro​',
        email: 'vajarami@bancolombia.com.co',
        vitrina: 'Nueva Movilidad Rionegro',
        marcas: 'Hyundai',
        direccion: 'Carrera 50 # 44A 86',
        ciudad: 'Rionegro'
    },
    {
        id: 38,
        concesionario: 'moevo',
        asesor: 'Jhon Alejandro Alzate Cardona',
        email: 'vajarami@bancolombia.com.co',
        vitrina: 'Nueva Movilidad Viva Envigado',
        marcas: 'Hyundai',
        direccion: 'Centro Comercial Viva Envigado Local 351A Nivel 3',
        ciudad: 'Medellín'
    },
    {
        id: 39,
        concesionario: 'mazko',
        asesor: 'Adriana Marcela Vega Benavides',
        email: 'admvega@bancolombia.com.co',
        vitrina: 'Mazko 3ra Norte',
        marcas: 'Mazda',
        direccion: 'AVDA 3 NORTE # 34 - 46',
        ciudad: 'Cali'
    },
    {
        id: 40,
        concesionario: 'Tuyomotor',
        asesor: 'Jorge Andrés Duque Pérez',
        email: 'JADUQUE@bancolombia.com.co',
        vitrina: 'Tuyomotor Poblado​',
        marcas: 'Toyota',
        direccion: 'Cra. 48 No. 14 -230 Av. Industriales​​​',
        ciudad: 'Medellín'
    }
]

const toggleCiudad = () => {
    let selectCiudad = document.getElementById('selectCiudad');

    selectCiudad.onclick = () => {
        let options = document.getElementById('optionsCiudad');
        options.classList.toggle('active');
    }
}

const toggleConcesionarios = () => {
    let selectConcesionarios = document.getElementById('selectConcesionario');

    selectConcesionarios.onclick = () => {
        let options = document.getElementById('optionsConcesionario');
        options.classList.toggle('active');
    }
}

const filterEvent = () => {
    for (let index = 0; index < concesionarios.length; index++) {
        const element = concesionarios[index];
        buildItemConcesionarios(element)
    }
    setPagination();
}

const buildItemConcesionarios = (element) => {
    let filterNew = document.getElementById('filterNew');
    filterNew.innerHTML += `
        <div class="itemFilter">
            <div class="headItem">
                <div class="iconHead">
                    <div class="itemIcon">
                        <svg width="22" height="25" viewBox="0 0 22 25" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M4.33334 1.33333C3.96515 1.33333 3.66668 1.63181 3.66668 2V23.3333H7.66668V16H14.3333V23.3333H18.3333V2C18.3333 1.63181 18.0349 1.33333 17.6667 1.33333H4.33334ZM13 23.3333V17.3333H9.00001V23.3333H13ZM2.33334 2V23.3333H0.333344V24.6667H2.33334H3.66668H18.3333H19.6667H21.6667V23.3333H19.6667V2C19.6667 0.895431 18.7712 0 17.6667 0H4.33334C3.22877 0 2.33334 0.895432 2.33334 2ZM7.00001 6C7.73639 6 8.33334 5.40305 8.33334 4.66667C8.33334 3.93029 7.73639 3.33333 7.00001 3.33333C6.26363 3.33333 5.66668 3.93029 5.66668 4.66667C5.66668 5.40305 6.26363 6 7.00001 6ZM11 6C11.7364 6 12.3333 5.40305 12.3333 4.66667C12.3333 3.93029 11.7364 3.33333 11 3.33333C10.2636 3.33333 9.66668 3.93029 9.66668 4.66667C9.66668 5.40305 10.2636 6 11 6ZM16.3333 4.66667C16.3333 5.40305 15.7364 6 15 6C14.2636 6 13.6667 5.40305 13.6667 4.66667C13.6667 3.93029 14.2636 3.33333 15 3.33333C15.7364 3.33333 16.3333 3.93029 16.3333 4.66667ZM7.00001 10C7.73639 10 8.33334 9.40305 8.33334 8.66667C8.33334 7.93029 7.73639 7.33333 7.00001 7.33333C6.26363 7.33333 5.66668 7.93029 5.66668 8.66667C5.66668 9.40305 6.26363 10 7.00001 10ZM12.3333 8.66667C12.3333 9.40305 11.7364 10 11 10C10.2636 10 9.66668 9.40305 9.66668 8.66667C9.66668 7.93029 10.2636 7.33333 11 7.33333C11.7364 7.33333 12.3333 7.93029 12.3333 8.66667ZM15 10C15.7364 10 16.3333 9.40305 16.3333 8.66667C16.3333 7.93029 15.7364 7.33333 15 7.33333C14.2636 7.33333 13.6667 7.93029 13.6667 8.66667C13.6667 9.40305 14.2636 10 15 10ZM8.33334 12.6667C8.33334 13.403 7.73639 14 7.00001 14C6.26363 14 5.66668 13.403 5.66668 12.6667C5.66668 11.9303 6.26363 11.3333 7.00001 11.3333C7.73639 11.3333 8.33334 11.9303 8.33334 12.6667ZM11 14C11.7364 14 12.3333 13.403 12.3333 12.6667C12.3333 11.9303 11.7364 11.3333 11 11.3333C10.2636 11.3333 9.66668 11.9303 9.66668 12.6667C9.66668 13.403 10.2636 14 11 14ZM16.3333 12.6667C16.3333 13.403 15.7364 14 15 14C14.2636 14 13.6667 13.403 13.6667 12.6667C13.6667 11.9303 14.2636 11.3333 15 11.3333C15.7364 11.3333 16.3333 11.9303 16.3333 12.6667Z"
                                fill="#2C2A29" />
                        </svg>
                    </div>
                </div>
                <div class="tituloHead">
                    <h6 class="titulo">
                        ${element.vitrina}
                    </h6>
                    <p>
                        ${element.ciudad}
                    </p>
                </div>
            </div>
            <div class="bodyItem">
                <div class="content">
                    <p>
                        Dirección: ${element.direccion}
                    </p>
                    <h6>
                    </h6>
                    <h6>
                        ${element.asesor}
                    </h6>
                    <a href="mailto:${element.email}">${element.email}</a>
                </div>
                <div class="marcas">
                    <h6>
                        Marcas <br>
                        <small>
                        ${element.marcas}
                        </small>
                    </h6>
                </div>
            </div>
        </div>
    `;
}

var filters = [];
const setFilter = (param) => {

    filters.push(param)
    filters.length > 3 ? alertLimitTags() : buildTag();
}

const buildTag = () => {
    let tagsFilters = document.getElementById('tagsFilters');
    tagsFilters.innerHTML = '';
    for (let index = 0; index < filters.length; index++) {
        const element = filters[index];
        tagsFilters.innerHTML += `
            <div class="contenedorFiltros" onclick="deleteTagFilter(event)" id="${element.type == 'ciudad' ? element.ciudad : element.concesionario}" data-type="${element.type}"> 
                <div> ${element.type == 'ciudad' ? element.ciudad : element.concesionario} </div>
                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M0.666656 11.138L1.13799 11.6093L6.13799 6.60933L11.138 11.6093L11.6093 11.138L6.60932 6.138L11.6093 1.138L11.138 0.666664L6.13799 5.66666L1.13799 0.666664L0.666656 1.138L5.66666 6.138L0.666656 11.138Z" fill="#2C2A29"/>
                </svg>

            </div>
        `;
    }
}

const alertLimitTags = () => {
    Swal.fire({
        icon: 'error',
        title: 'Lo siento',
        text: 'Solo puedes escoger 3 filtros',
    })
}

const deleteTagFilter = (event) => {
    const tagElement = {
        idElement: event.srcElement.id,
        type: event.srcElement.dataset.type
    }
    filters = removeItemFromArr(filters, tagElement);
    buildTag();
    filterEventClick();
}

removeItemFromArr = (arr, item) => {
    item.type == 'ciudad' ?
        itemSplit = arr.filter(e => e.ciudad !== item.idElement) :
        itemSplit = arr.filter(e => e.concesionario !== item.idElement)
    return itemSplit;
};


var filterConcesionarios = []
const filterEventClick = () => {
    //Si el filtro solo tiene un parámetro
    if (filters.length == 1) {
        if (filters[0].type == 'ciudad') {
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad
                );

        } else {
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.concesionario == filters[0].concesionario
                );
        }
    }
    // si el filtro tiene dos parámetros 
    else if (filters.length == 2) {
        if (filters[0].type == 'ciudad' && filters[1].type == 'ciudad') {
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad && n.ciudad == filters[1].ciudad
                );
        } else if (filters[0].type == 'concesionario' && filters[1].type == 'concesionario') {
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.concesionario == filters[0].concesionario && n.concesionario == filters[1].concesionario
                );
        } else if (filters[0].type == 'ciudad' && filters[1].type == 'concesionario') {
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad && n.concesionario == filters[1].concesionario
                );
        } else if (filters[0].type == 'concesionario' && filters[1].type == 'ciudad') {
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.concesionario == filters[0].concesionario && n.ciudad == filters[1].ciudad
                );
        }
    }
    else if (filters.length == 3) {
        console.log("holi")
        if (filters[0].type == 'ciudad' && filters[1].type == 'ciudad' && filters[2].type == 'ciudad') {
            console.log("solo ciudades")
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad && n.ciudad == filters[1].ciudad && n.ciudad == filters[2].ciudad
                );
        } else if (filters[0].type == 'concesionario' && filters[1].type == 'concesionario' && filters[2].type == 'concesionario') {
            console.log("solo concesionarios")
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.concesionario == filters[0].concesionario && n.concesionario == filters[1].concesionario && n.concesionario == filters[2].concesionario
                );
        } else if (filters[0].type == 'ciudad' && filters[1].type == 'ciudad' && filters[2].type == 'concesionario') {
            console.log(`ciudad ${filters[0].ciudad}, ciudad ${filters[1].ciudad} y concesionario ${filters[2].concesionario}`)
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad && n.ciudad == filters[1].ciudad && n.concesionario == filters[2].concesionario
                );
        } else if (filters[0].type == 'concesionario' && filters[1].type == 'ciudad' && filters[2].type == 'ciudad') {
            console.log(`concesionario ${filters[0].concesionario}, ciudad ${filters[1].ciudad} y ciudad ${filters[2].ciudad}`)
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.concesionario == filters[0].concesionario && n.ciudad == filters[1].ciudad && n.ciudad == filters[2].ciudad
                );
        } else if (filters[0].type == 'concesionario' && filters[1].type == 'ciudad' && filters[2].type == 'concesionario') {
            console.log("concesionario, ciudad y concesionario")
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.concesionario == filters[0].concesionario && n.ciudad == filters[1].ciudad && n.ciudad == filters[2].ciudad
                );
        } else if (filters[0].type == 'ciudad' && filters[1].type == 'concesionario' && filters[2].type == 'concesionario') {
            console.log("concesionario, ciudad y concesionario")
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad && n.concesionario == filters[1].concesionario && n.concesionario == filters[2].concesionario
                );
        }
        else if (filters[0].type == 'ciudad' && filters[1].type == 'concesionario' && filters[2].type == 'ciudad') {
            console.log("ciudad, concesionario y ciudad")
            filterConcesionarios = concesionarios
                .filter(n =>
                    n.ciudad == filters[0].ciudad && n.concesionario == filters[1].concesionario && n.concesionario == filters[2].concesionario
                );
        }
    } else {
        destroyPagination();
        filterEvent();
        mostrarCantidadResultados(concesionarios)
        return
    }
    destroyPagination();
    filterNew.innerHTML = '';
    for (let index = 0; index < filterConcesionarios.length; index++) {
        const element = filterConcesionarios[index];
        buildItemConcesionarios(element)
    }

    setPagination();
    mostrarCantidadResultados(filterConcesionarios)
}

const getCiudades = () => {
    var ciudades = [];
    let result = [];
    for (let index = 0; index < concesionarios.length; index++) {
        const element = concesionarios[index];
        result.push(element.ciudad);
    }
    ciudades = result.filter((item, index) => {
        return result.indexOf(item) === index;
    })
    let options = document.getElementById('optionsCiudad');
    for (let index = 0; index < ciudades.length; index++) {
        const element = ciudades[index];
        options.innerHTML += `
            <div class="optionEelement">
                <div onclick="setFilter({ciudad:'${element}', type: 'ciudad'})">
                    <p> ${element} </p>
                </div>
            </div>
        
        `;
    }
}


const mostrarCantidadResultados = (element) => {
let cantidadResultados = document.getElementById('cantidadResultados');
    cantidadResultados.innerHTML = '';
    cantidadResultados.innerHTML += `<div class="cantidad"> <h6> Tus filtros  </h6> <h6 class="lightTitle">    ${element.length} resultados</h6> </div>`
}

const getConcesionarios = () => {
    var concesionariosArray = [];
    let result = [];
    for (let index = 0; index < concesionarios.length; index++) {
        const element = concesionarios[index];
        result.push(element.concesionario);
    }
    concesionariosArray = result.filter((item, index) => {
        return result.indexOf(item) === index;
    })
    let options = document.getElementById('optionsConcesionario');
    for (let index = 0; index < concesionariosArray.length; index++) {
        const element = concesionariosArray[index];
        options.innerHTML += `
            <div class="optionEelement">
                <div onclick="setFilter({concesionario:'${element}', type: 'concesionario'})">
                    <p> ${element} </p>
                </div>
            </div>
        
        `;
    }
}

//Tabs 
const borrarElmentosTabs = () => {
    let arrowAwesome = `
        <svg width="18" height="10" viewBox="0 0 18 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M17.092 0.293007L9.607 7.77801C9.216 8.16801 8.583 8.16801 8.193 7.77801L0.707 0.293007L0 1.00001L7.486 8.48501C8.265 9.26501 9.534 9.26501 10.314 8.48501L17.799 1.00001L17.092 0.293007Z" fill="#2C2A29"/>
        </svg>    
    `;
    const arrowsTabs = document.querySelectorAll('.pull-right.glyphicon.glyphicon-plus');
    const itemsTabs = document.getElementsByClassName('titulo_acordeon');
    for (let index = 0; index < arrowsTabs.length; index++) {
        const element = arrowsTabs[index];
        element.parentNode.removeChild(element);
    }

    for (let index = 0; index < itemsTabs.length; index++) {
        const element = itemsTabs[index];
        element.innerHTML += `${arrowAwesome}`;

    }
}

window.addEventListener('DOMContentLoaded', (event) => {
    filterEvent();
    getCiudades();
    getConcesionarios();
    toggleConcesionarios();
    toggleCiudad();
    borrarElmentosTabs();
});

//pagination
var pagination = $('.pagination');
function setPagination() {
    if(screen.width > 768) {
        pagination.jPages({
            containerID: 'filterNew',
            animation: 'fadeInUp',
            fallback: 'slow',
            perPage: 12,
            startPage: 1,
            startRange: 1,
            midRange: 1,
            endRange: 1,
            first: true,
            last: false
        });
        addArrows();
    } else {
        pagination.jPages({
            containerID: 'filterNew',
            animation: 'fadeInUp',
            fallback: 'slow',
            perPage: 4,
            startPage: 1,
            startRange: 1,
            midRange: 1,
            endRange: 1,
            first: true,
            last: false
        });
        addArrows();
    }
}

function destroyPagination() {
    pagination.jPages('destroy');
};

const addArrows = () => {
    const previous = document.querySelector('.jp-previous');
    const next = document.querySelector('.jp-next');
    const arrow = `
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill="#FFF" fill-rule="evenodd" clip-rule="evenodd" d="M7.852 13.3947L7.38067 13.866L2.39067 8.876C2.13867 8.624 2 8.28933 2 7.93333C2 7.57667 2.13867 7.242 2.39067 6.99067L7.38067 2L7.852 2.47133L2.862 7.462C2.82021 7.50379 2.78466 7.55005 2.7558 7.59973H14.0473V8.2664H2.75557C2.78447 8.31631 2.8201 8.36277 2.862 8.40467L7.852 13.3947Z" fill="#2C2A29"/>
            <mask id="mask0_160_157" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="2" y="2" width="13" height="12">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M7.852 13.3947L7.38067 13.866L2.39067 8.876C2.13867 8.624 2 8.28933 2 7.93333C2 7.57667 2.13867 7.242 2.39067 6.99067L7.38067 2L7.852 2.47133L2.862 7.462C2.82021 7.50379 2.78466 7.55005 2.7558 7.59973H14.0473V8.2664H2.75557C2.78447 8.31631 2.8201 8.36277 2.862 8.40467L7.852 13.3947Z" fill="white"/>
            </mask>
            <g mask="url(#mask0_160_157)">
            <rect width="16" height="16" fill="white"/>
            </g>
        </svg>
    `;
    previous.innerHTML = '';
    next.innerHTML = '';
    previous.innerHTML += `${arrow}`;
    next.innerHTML += `${arrow}`;
}